#README

### This Repo is now deprecated

Thank's for the interest in the FormBinder utility. This Java class is meant to be used with the Play! Framework for binding transfering property valuse from `Form<>` entities to `play.db.ebean.Model` classes.

### Usage

Model class:

```
//Imports
public class User extends Model {
    @Id
    public Long id;
    public String email;
    pblic String firstName;
    public String lastName;
    @Transient
    public String password;
    //1000 other properties
}
```
Form class:

```
//Imports
public class UserForm extends FormBinder {
    //Ensure that the field names in this class match the field names for the model you're
    //trying to map to
    @Constriants.Required
    public String email;
    @Constraints.Required
    public String firstName;
    @Constraints.Required
    public String lastName;
    @Constraints.Required
    public String password;
    //Other properties
}
```

The Controller:

```
//Imports
public static Result createUser() {
    Form<UserForm> filledForm = Form.form(UserForm.class).bindFromRequest();
    if(filledForm.hasErrors()) { 
        //Handle the error 
    }
    User user = new User();
    filledForm.get().writeTo(user); //There's the magic!
    user.save();
    return ok(Json.toJson(user));
}
```
As you can see it ties in pretty well to the Play Framework Form workflow you're used to; all it takes is an extension of your usual Form POJO.

### Usage Rights

This project is free to use for anyone on any project. Fork the repo at [https://bitbucket.org/revise_labs/formbinder](https://bitbucket.org/revise_labs/formbinder) and go wild or commit to the existing one, no problem

This file was created by Kevin Sheppard of Revise Labs.